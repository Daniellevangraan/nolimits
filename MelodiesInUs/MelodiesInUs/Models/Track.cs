﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MelodiesInUs.Models;

namespace MelodiesInUs.Models
{
    public class Track : IEnumerable
    {
        public dynamic id { get; set; }
        public string artist { get; set; }
        public string title { get; set; }
        public HttpPostedFileBase File { get; set; }
        public IEnumerator GetEnumerator()
        {
            throw new NotImplementedException();
        }
    }
}