﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MongoDB.Driver;
namespace MelodiesInUs.Models
{

    public class MelodiesInUsContext 
    {
        public MongoDatabaseBase Db { get; set; }
        public MelodiesInUsContext()
        {

            var settings = new MongoClientSettings();
            settings.Server = new MongoServerAddress("localhost",27017);
            MongoClient client = new MongoClient();
            this.Db = (MongoDatabaseBase)client.GetDatabase("Music");
            
        }
    }

}