﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Services.Description;
using System.Web.WebPages;
using MelodiesInUs.Models;
using Microsoft.Ajax.Utilities;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Bson.Serialization;
using Newtonsoft.Json;
using MongoDB.Driver.Linq;
using MongoDB.Driver.Support;

namespace MelodiesInUs.Controllers
{
    public class TrackController : Controller
    {
        // GET: Track
        public async Task<ViewResult> Index()
        {
            var musicdb = new MelodiesInUsContext();
            var trackList = new TrackList();
            await GetListOfTracks(musicdb, trackList);
            return View(trackList);
        }

        private static async Task GetListOfTracks(MelodiesInUsContext musicdb, TrackList trackList)
        {
            using (
                var cursor =
                    await musicdb.Db.GetCollection<BsonDocument>("Track").Find(new BsonDocument()).ToCursorAsync())
            {
                while (await cursor.MoveNextAsync())
                {
                    foreach (var doc in cursor.Current)
                    {
                        Track t = new Track();
                        t.id = doc["_id"];
                        t.title = !doc["title"].IsBsonNull ? (string) doc["title"] : "";
                        t.artist = !doc["artist"].IsBsonNull ? (string) doc["artist"] : "";
                        trackList.ListOfTracks.Add(t);
                    }
                }
            }
        }

        // GET: Track/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Track/Create
        public ActionResult Create()
        {
           // var musicdb = new MelodiesInUsContext();
            //var db = music.Db;
            //Track track = new Track();
            //track._id = "1";
            //track._title = "Leelaloo";
            //var collection = db.GetCollection<Track>("Track");

            //collection.InsertOne(track);
            return View();
        }



        // POST: Track/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            var path = "";
            var artist = collection.GetValue("artist");
            var title = collection.GetValue("title");
            var uri = collection.GetValues("File"); 

            path = uri == null? "~/Content/Audio/" + uri[0]:"no path";
          
            var musicdb = new MelodiesInUsContext();
            try
            {
                BsonDocument track = new BsonDocument
                {
                    {"artist", artist.AttemptedValue},
                    {"title", title.AttemptedValue},
                    {"uri", path}
                };
                var coll = musicdb.Db.GetCollection<BsonDocument>("Track");
                coll.InsertOne(track);

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }



        // GET: Track/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Track/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Track/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Track/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //[HttpPost]
        //public HttpPostedFileBase CreateFile(HttpPostedFileBase file)
        //{

        //    try
        //    {
        //        if (file != null && file.ContentLength > 0)
        //        {
        //            var fileName = Path.GetFileName(file.FileName);
        //            var path = Path.Combine(Server.MapPath("~/Content/Audio/"), fileName);
        //            file.SaveAs(path);

        //            //return fileName;
        //        }
        //    }
        //    catch (Exception e)
        //    {
        //        Console.WriteLine(e);
        //        throw;
        //    }


        //    //return file;

        //}
    }
}
